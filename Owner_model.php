<?php
/**
  * Clase Owner_model
  * 
  *  Métodos para hacer diferentes consultas en la base de datos.
  * @author Edwin Omar Poot Díaz<edwin.poot.diaz@gmail.com> 
  * @copyright  2016 
  */
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                $this->load->database();
        }
/**
  * Función para obtener los datos de la propiedad que le corresponde al propietario.
  * @return array
  */
        public function get_todos()
        {
                $user = $this->session->userdata('username');
                $query = $this->db->where('owner', $user);
                $query = $this->db->get('dates_houses');
                return $query->result();
        }
/**
  * Función para obtener todos los datos de la propiedades reservadas.
  * @return array
  */
        public function get_reservations()
        {
                $query = $this->db->get('dates_houses');
                return $query->result();
        } 
/**
  * Función para obtener todos los datos de los propietarios.
  * @return array
  */
        public function get_owners()
        {
             $query = $this->db->get('owners');
             if ($query->num_rows() > 0) {
                foreach($query->result() as $row)
                $arrDatos[htmlspecialchars($row->names, ENT_QUOTES)] = 
                htmlspecialchars($row->names, ENT_QUOTES);
                $query->free_result();
                return $arrDatos;
             }  

        }
/**
  * Función para obtener todos los datos de las propiedades rentadas.
  * @return array
  */
        public function get_properties()
        {
             $query = $this->db->get('rentals');
            if ($query->num_rows() > 0) {
                foreach($query->result() as $row)
                $arrProperties[htmlspecialchars($row->name, ENT_QUOTES)] = 
                htmlspecialchars($row->name, ENT_QUOTES);
                $query->free_result();
                return $arrProperties;
             }
        }
/**
  * Función para agregar nuevos registros en la db.
  * @return array
  */
        public function add()
        {
                $data_insertar = $this->input->post();
                unset($data_insertar['btn_enviar']);
                $this->db->insert('dates_houses', $data_insertar);
                return $this->db->insert_id();
        }
/**
  * Función para obtener el id especifico de la propiedad.
  * @return array
  */
        public function get_by_id($id)
        {
                $query = $this->db->where('id', $id);
                $query = $this->db->get('dates_houses');
                return $query->result();
        }
/**
  * Función para editar los datos de las propiedades.
  * @return array
  */
        public function edit($id)
        {
                $data_editar = $this->input->post();
                unset($data_editar['btn_enviar']);
                $this->db->where('id', $id);
                $this->db->update('dates_houses', $data_editar);
        }

/**
  * Función para eliminar los datos de las propiedades.
  * @return array
  */
   
        public function elim($id)
        {
                $this->db->where('id', $id);
                $this->db->delete('dates_houses');
        }

}

/* Fin del archivo Owner_model.php */