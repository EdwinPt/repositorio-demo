<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_reservation extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('Owner_model');
    }

    public function index()
    {
        if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') == 'propietario')
        {
            redirect(base_url().'login');
        }

        //ADMINISTRADOR
        if ($this->session->userdata('perfil') == 'administrador') {
          $data['listado'] = $this->Owner_model->get_reservations();
          $this->load->view('header');
          $this->load->view('menu2');
          $this->load->view('admin_reservation_view', $data);
          $this->load->view('footer2');
        }

        //EDITOR
        if ($this->session->userdata('perfil') == 'editor') {
          $data['listado'] = $this->Owner_model->get_reservations();
          $this->load->view('header');
          $this->load->view('menueditor');
          $this->load->view('admin_reservation_view', $data);
          $this->load->view('footer2');
        }
    }

    public function mis_reglas()
    {
        $this->form_validation->set_rules('property', 'Property', 'required');
        $this->form_validation->set_rules('start_date', 'Start date', 'required');
        $this->form_validation->set_rules('end_date', 'End date', 'required');
        $this->form_validation->set_rules('owner', 'Owner', 'required');

    }

    public function add()
	  {
        if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') == 'propietario')
        {
            redirect(base_url().'login');
        }

        //ADMINISTRADOR
        if ($this->session->userdata('perfil') == 'administrador') {
          if ($this->input->post())
          {
              $this->mis_reglas();
              if ($this->form_validation->run() == TRUE)
              {
                  $id_insertado = $this->Owner_model->add();
                  redirect('admin_reservation');
              }
              else
              {
                  $data['arrOwners'] = $this->Owner_model->get_owners();
                  $data['arrProperty'] = $this->Owner_model->get_properties();
                  $this->load->view('header');
                  $this->load->view('menu2');
                  $this->load->view('admin_reservation_form_view', $data);
                  $this->load->view('footer2');
              }
          }
          else
          {
              $data['arrOwners'] = $this->Owner_model->get_owners();
              $data['arrProperty'] = $this->Owner_model->get_properties();
              $this->load->view('header');
              $this->load->view('menu2');
          	$this->load->view('admin_reservation_form_view', $data);
              $this->load->view('footer2');
          }
        }

        //EDITOR
        if ($this->session->userdata('perfil') == 'editor') {
          if ($this->input->post())
          {
              $this->mis_reglas();
              if ($this->form_validation->run() == TRUE)
              {
                  $id_insertado = $this->Owner_model->add();
                  redirect('admin_reservation');
              }
              else
              {
                  $data['arrOwners'] = $this->Owner_model->get_owners();
                  $data['arrProperty'] = $this->Owner_model->get_properties();
                  $this->load->view('header');
                  $this->load->view('menueditor');
                  $this->load->view('admin_reservation_form_view', $data);
                  $this->load->view('footer2');
              }
          }
          else
          {
              $data['arrOwners'] = $this->Owner_model->get_owners();
              $data['arrProperty'] = $this->Owner_model->get_properties();
              $this->load->view('header');
              $this->load->view('menueditor');
          	$this->load->view('admin_reservation_form_view', $data);
              $this->load->view('footer2');
          }
        }
    }

    public function edit($id = NULL)
    {
        if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') == 'propietario')
        {
            redirect(base_url().'login');
        }

        if ($id == NULL OR !is_numeric($id))
        {
            echo "Error con el ID";
            return;
        }

        //ADMINISTRADOR
        if ($this->session->userdata('perfil') == 'administrador') {
          if ($this->input->post())
          {
              $this->mis_reglas();
              if ($this->form_validation->run() == TRUE)
              {
                  $this->Owner_model->edit($id);
                  redirect('admin_reservation');
              }
              else
              {
                  $data['datos_rentals'] = $this->Owner_model->get_by_id($id);
                  $data['arrOwners'] = $this->Owner_model->get_owners();
                  $data['arrProperty'] = $this->Owner_model->get_properties();
                  $this->load->view('header');
                  $this->load->view('menu2');
                  $this->load->view('admin_reservation_form_view', $data);
                  $this->load->view('footer2');
              }
          }
          else
          {
              $data['datos_rentals'] = $this->Owner_model->get_by_id($id);
              $data['arrOwners'] = $this->Owner_model->get_owners();
              $data['arrProperty'] = $this->Owner_model->get_properties();
              if (empty($data['datos_rentals']))
              {
                  echo "El ID es invalido";
              }
              else
              {
                  $this->load->view('header');
                  $this->load->view('menu2');
                  $this->load->view('admin_reservation_form_view', $data);
                  $this->load->view('footer2');
              }
          }
        }

        //EDITOR
        if ($this->session->userdata('perfil') == 'editor') {
          if ($this->input->post())
          {
              $this->mis_reglas();
              if ($this->form_validation->run() == TRUE)
              {
                  $this->Owner_model->edit($id);
                  redirect('admin_reservation');
              }
              else
              {
                  $data['datos_rentals'] = $this->Owner_model->get_by_id($id);
                  $data['arrOwners'] = $this->Owner_model->get_owners();
                  $data['arrProperty'] = $this->Owner_model->get_properties();
                  $this->load->view('header');
                  $this->load->view('menueditor');
                  $this->load->view('admin_reservation_form_view', $data);
                  $this->load->view('footer2');
              }
          }
          else
          {
              $data['datos_rentals'] = $this->Owner_model->get_by_id($id);
              $data['arrOwners'] = $this->Owner_model->get_owners();
              $data['arrProperty'] = $this->Owner_model->get_properties();
              if (empty($data['datos_rentals']))
              {
                  echo "El ID es invalido";
              }
              else
              {
                  $this->load->view('header');
                  $this->load->view('menueditor');
                  $this->load->view('admin_reservation_form_view', $data);
                  $this->load->view('footer2');
              }
          }
        }
    }

    public function delete($id = NULL)
    {
        if($this->session->userdata('perfil') == FALSE || $this->session->userdata('perfil') == 'propietario')
        {
            redirect(base_url().'login');
        }

        if ($id == NULL OR !is_numeric($id))
        {
            echo "Error con el ID";
            return;
        }

        //ADMINISTRADOR
        if ($this->session->userdata('perfil') == 'administrador') {
          if ($this->input->post())
          {
              $id_eliminar = $this->input->post('id');
              $this->Owner_model->elim($id_eliminar);
              redirect('admin_reservation');
          }
          else
          {
              $data['datos_rentals'] = $this->Owner_model->get_by_id($id);
              if (empty($data['datos_rentals']))
              {
                  echo "El ID es invalido";
              }
              else
              {
                  $this->load->view('header');
                  $this->load->view('menu2');
                  $this->load->view('admin_reservation_delete_view', $data);
                  $this->load->view('footer2');
              }
          }
        }

        //EDITOR
        if ($this->session->userdata('perfil') == 'editor') {
          if ($this->input->post())
          {
              $id_eliminar = $this->input->post('id');
              $this->Owner_model->elim($id_eliminar);
              redirect('admin_reservation');
          }
          else
          {
              $data['datos_rentals'] = $this->Owner_model->get_by_id($id);
              if (empty($data['datos_rentals']))
              {
                  echo "El ID es invalido";
              }
              else
              {
                  $this->load->view('header');
                  $this->load->view('menueditor');
                  $this->load->view('admin_reservation_delete_view', $data);
                  $this->load->view('footer2');
              }
          }
        }
    }
}

/* Fin del archivo Admin_reservation.php */
