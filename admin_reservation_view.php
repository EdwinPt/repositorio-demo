<div class="container">
	<div class="row">

		<div class="table-responsive">

			<div class="col-xs-12 col-sm-12 col-md-12">
			<h3>Reservations</h3>
			<br>
			    <?php if (empty($listado)) { ?>
						<p><strong>There are no registers</strong></p>
				<?php } else { ?>
					<ul class="nav nav-pills">
						<li class="active">
							<a href="javascript:void(0)" style="background-color: #009688;">Total of registers:
								<span class="badge" style="color: #009688;"><?php echo count($listado); ?></span>
							</a>
						</li>
					</ul>
				<?php } ?>
				
				<?php if (empty($listado)) { ?>
						<h1>Sin registro</h1>
					<?php}else{ ?>

				<table class="table">
					<caption>
						Registers management.
						<a href="<?php echo base_url('/admin_reservation/add');?>" class="btn btn-raised btn-default">Add register</a>
					</caption>
					<thead>
						<tr>
							<th>#</th>
							<th>Property</th>
							<th>Start date</th>
							<th>End date</th>
							<th>Owner</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
					
					<?php foreach($listado as $registro){?>
					
						<tr>
							<th scope="row"><?php echo $registro->id; ?></th>
							<td><?php echo $registro->property; ?></td>
							<td><?php echo $registro->start_date; ?></td>
							<td><?php echo $registro->end_date; ?></td>
							<td><?php echo $registro->owner; ?></td>
							<td>
								<div class="btn-group-vertical">
									<a href="<?php echo base_url('/admin_reservation/delete/'.$registro->id);?>" class="btn btn-raised btn-xs">Delete</a>
									<a href="<?php echo base_url('/admin_reservation/edit/'.$registro->id);?>" class="btn btn-raised btn-xs">Update</a>
								</div>
							</td>
						</tr>
					<?php
							}
						}
					?>
					</tbody>
				</table>

			</div>

		</div>

	</div>
</div>
